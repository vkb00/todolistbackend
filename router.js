const Router = require('express');
const router = new Router();
const controller = require('./todoController')

router.post('/task', controller.addTask);
router.put('/task/:id',controller.updateTask);
router.delete('/task/:id',controller.deleteTask);
router.get('/task',controller.getTasks);

module.exports = router;
