const Task = require('./models/Task');
class todoController {
    async addTask(req, res) {
        try {
            console.log(req.body);
            const task = new Task(req.body);
            await task.save();
            res.json(task)
        }
        catch (e) {
            res.status(500).json({
                message: "error",
            });
        }
    }
    async updateTask(req, res) {
        try {
            console.log(req.body);
            await Task.findOneAndUpdate({ _id: req.params.id }, req.body.newData);
            res.status(200).json({
                message: "updated",
            });
        }
        catch (e) {
            res.status(500).json({
                message: "error",
            });
        }
    }
    async deleteTask(req, res) {
        try {
            console.log(req.params.id);
            await Task.findByIdAndDelete(req.params.id)
            res.status(200).json({
                message: "deleted",
            });
        }
        catch (e) {
            res.status(500).json({
                message: "error",
            });
        }
    }
    async getTasks(req, res) {
        try {
            const tasks = await Task.find();
            res.json(tasks)

        }
        catch (e) {
            res.status(500).json({
                message: "error",
            });
        }
    }
}
module.exports = new todoController();