const { Schema, model } = require('mongoose');

const Task = new Schema({
    title: { type: String, require: true },
    description: { type: String, require: true },
    status: { type: String, require: true }
})
module.exports = model('Task', Task);