const express = require('express');
const mongoose = require('mongoose');
const router = require('./router')
const PORT = process.env.PORT || 5000;
const app = express();

app.use(express.json());
app.use('/todo', router)

const start = async () => {
    try {
        await mongoose.connect("mongodb+srv://vkb00:vkb00@cluster0.uhbnzgl.mongodb.net/")
        app.listen(PORT, () => console.log('Server started'))
    }
    catch (e) {

    }
}
start();